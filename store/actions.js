const actions = {
    updatePostsCurrentPage ({ commit }, value) {
        commit('updatePostsCurrentPage', value)
    },
    updatePaginationNextPage ({ commit }, value) {
        commit('updatePaginationNextPage', value)
    },
    updatePaginationTo ({ commit }, value){
        commit('updatePaginationTo', value)
    },
    updatePaginationFrom ({ commit }, value){
        commit('updatePaginationFrom', value)
    },
    updatePaginationPreviousPage ({ commit }, value){
        commit('updatePaginationPreviousPage', value)
    },
    updatePaginationTotal ({ commit }, value){
        commit('updatePaginationTotal', value)
    },
    updatePaginationTotalPages ({ commit }, value){
        commit('updatePaginationTotalPages', value)
    }
}
  
export default actions