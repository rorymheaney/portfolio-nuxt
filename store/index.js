import Vuex from 'vuex'
import mutations from './mutations'
import actions from './actions'

const createStore = () => {
    return new Vuex.Store({
        state: {
            pageTemplate: '',
            isNavActive: false,
            pageClass: '',
            posts_page: [],
            postsData: {
                per_page: 4,
                page: 1
            },
            pagination: {
                prevPage: '',
                nextPage: 2,
                totalPages: '',
                from: 1,
                to: 4,
                total: '',
            }
        },
        mutations,
        actions
    })
}

export default createStore