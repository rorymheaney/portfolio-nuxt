"use strict"

const API_CONFIG = {
    baseUrl: 'https://portfolio.fancyapps.blog/wp-json/',
    basePostsUrl: 'wp/v2/posts',
    basePagesUrl: 'wp/v2/pages',
    baseProjectsUrl: 'wp/v2/projects'
}

module.exports = API_CONFIG;