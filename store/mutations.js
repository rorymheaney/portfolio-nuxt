const mutations = {
    mainClassChange(state) {
        state.mainClass
    },
    pageTemplateChange(state) {
        state.pageTemplate
    },
    updatePostsCurrentPage (state, value) {
        // console.log('set current page value', value)
        state.postsData.page = value
    },
    updatePaginationNextPage (state, value) {
        // console.log('set next page value', value)
        state.pagination.nextPage = value
    },
    updatePaginationTo (state, value){
        state.pagination.to = value
    },
    updatePaginationFrom (state, value){
        state.pagination.from = value
    },
    updatePaginationPreviousPage (state, value){
        // console.log(value);
        state.pagination.prevPage = value
    },
    updatePaginationTotal (state, value){
        state.pagination.total = value
    },
    updatePaginationTotalPages (state, value){
        state.pagination.totalPages = value
        // console.log(value);
    }
}
  
export default mutations