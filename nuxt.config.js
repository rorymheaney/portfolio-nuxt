const pkg = require('./package')
const webpack = require('webpack')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/what-input/5.1.1/what-input.min.js'},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/foundation/6.5.0-rc.2/js/foundation.min.js'},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.23.0/polyfill.min.js'}
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#000000',
    height: '5px' 
   },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/scss/main.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    ['@nuxtjs/sitemap', {
      path: '/sitemap.xml',
      hostname: 'https://fancysquares.blog',
      generate: false,
      routes: [
        "/projects",
        "/posts",
        "/contact",
        "/wpseattle",
        "/",
        "/posts/wordpress-ajax-request-with-axios-timber-templating",
        "/posts/wordpress-password-protected-data-in-endpoints",
        "/posts/start-extending-the-wordpress-api",
        "/posts/wordpress-rest-api-get-random-post-endpoint",
        "/posts/how-to-get-the-featured-image-url-in-wp-api-v2",
        "/posts/add-custom-post-types-to-wp-api-v2",
        "/posts/add-featured-image-as-a-background-image",
        "/posts/add-sequential-classes-to-loop",
        "/posts/wp-rest-api-wp-api-tips-and-tricks",
        "/posts/wordpress-insert-clearing-div-every-3-posts-while-loop-foreach-loop",
        "/posts/display-posts-from-wordpress-custom-post-type",
        "/posts/woocommerce-get-lowest-price-category-archive-archive-product-php",
        "/posts/advanced-custom-fields-repeater-pagination",
        "/posts/display-recent-posts-specific-category-in-wordpress",
        "/posts/advanced-custom-fields-repeater-wrap-every-3-divs-row",
        "/posts/google-maps-infowindow-advanced-custom-fields-acf",
        "/posts/wordpress-if-statements",
        "/posts/magento-simple-configurable-products",
        "/posts/show-magento-custom-attribute",
        "/posts/magento-if-statements",
        "/posts/ubermenu-working-roots-theme",
        "/posts/open-fancybox-on-page-load",
        "/posts/limit-amount-of-checkboxes-that-be-checked-in-gravity-forms",
        "/posts/advanced-media-search-wordpress",
        "/posts/gravity-forms-tab-going-next-field",
        "/posts/wordpress-search-and-replace-tool",
        "/posts/break-time",
        "/posts/essential-wordpress-plugins-for-a-strong-website",
        "/posts/roots-wordpress-theme"
      ]
    }],
    ['@nuxtjs/google-analytics', {
      id: 'UA-66114298-1'
    }],
    ['@nuxtjs/redirect-module', [ 
      /* Redirect option here */
        { from: '^/wordpress-ajax-request-with-axios-timber-templating', to: '/posts/wordpress-ajax-request-with-axios-timber-templating', statusCode: 301 },
        { from: '^/wordpress-password-protected-data-in-endpoints', to: '/posts/wordpress-password-protected-data-in-endpoints', statusCode: 301 },
        { from: '^/start-extending-the-wordpress-api', to: '/posts/start-extending-the-wordpress-api', statusCode: 301 },
        { from: '^/wordpress-rest-api-get-random-post-endpoint', to: '/posts/wordpress-rest-api-get-random-post-endpoint', statusCode: 301 },
        { from: '^/how-to-get-the-featured-image-url-in-wp-api-v2', to: '/posts/how-to-get-the-featured-image-url-in-wp-api-v2', statusCode: 301 },
        { from: '^/add-custom-post-types-to-wp-api-v2', to: '/posts/add-custom-post-types-to-wp-api-v2', statusCode: 301 },
        { from: '^/add-featured-image-as-a-background-image', to: '/posts/add-featured-image-as-a-background-image', statusCode: 301 },
        { from: '^/add-sequential-classes-to-loop', to: '/posts/add-sequential-classes-to-loop', statusCode: 301 },
        { from: '^/wp-rest-api-wp-api-tips-and-tricks', to: '/posts/wp-rest-api-wp-api-tips-and-tricks', statusCode: 301 },
        { from: '^/wordpress-insert-clearing-div-every-3-posts-while-loop-foreach-loop', to: '/posts/wordpress-insert-clearing-div-every-3-posts-while-loop-foreach-loop', statusCode: 301 },
        { from: '^/display-posts-from-wordpress-custom-post-type', to: '/posts/display-posts-from-wordpress-custom-post-type', statusCode: 301 },
        { from: '^/woocommerce-get-lowest-price-category-archive-archive-product-php', to: '/posts/woocommerce-get-lowest-price-category-archive-archive-product-php', statusCode: 301 },
        { from: '^/advanced-custom-fields-repeater-pagination', to: '/posts/advanced-custom-fields-repeater-pagination', statusCode: 301 },
        { from: '^/display-recent-posts-specific-category-in-wordpress', to: '/posts/display-recent-posts-specific-category-in-wordpress', statusCode: 301 },
        { from: '^/advanced-custom-fields-repeater-wrap-every-3-divs-row', to: '/posts/advanced-custom-fields-repeater-wrap-every-3-divs-row', statusCode: 301 },
        { from: '^/google-maps-infowindow-advanced-custom-fields-acf', to: '/posts/google-maps-infowindow-advanced-custom-fields-acf', statusCode: 301 },
        { from: '^/wordpress-if-statements', to: '/posts/wordpress-if-statements', statusCode: 301 },
        { from: '^/magento-simple-configurable-products', to: '/posts/magento-simple-configurable-products', statusCode: 301 },
        { from: '^/show-magento-custom-attribute', to: '/posts/show-magento-custom-attribute', statusCode: 301 },
        { from: '^/magento-if-statements', to: '/posts/magento-if-statements', statusCode: 301 },
        { from: '^/ubermenu-working-roots-theme', to: '/posts/ubermenu-working-roots-theme', statusCode: 301 },
        { from: '^/open-fancybox-on-page-load', to: '/posts/open-fancybox-on-page-load', statusCode: 301 },
        { from: '^/limit-amount-of-checkboxes-that-be-checked-in-gravity-forms', to: '/posts/limit-amount-of-checkboxes-that-be-checked-in-gravity-forms', statusCode: 301 },
        { from: '^/advanced-media-search-wordpress', to: '/posts/advanced-media-search-wordpress', statusCode: 301 },
        { from: '^/gravity-forms-tab-going-next-field', to: '/posts/gravity-forms-tab-going-next-field', statusCode: 301 },
        { from: '^/wordpress-search-and-replace-tool', to: '/posts/wordpress-search-and-replace-tool', statusCode: 301 },
        { from: '^/break-time', to: '/posts/break-time', statusCode: 301 },
        { from: '^/essential-wordpress-plugins-for-a-strong-website', to: '/posts/essential-wordpress-plugins-for-a-strong-website', statusCode: 301 },
        { from: '^/roots-wordpress-theme', to: '/posts/roots-wordpress-theme', statusCode: 301 },
        { from: '^/admin', to: 'https://portfolio.fancyapps.blog/wp-admin/', }
      ]
    ],
    [
      "nuxt-mq",
      {
        breakpoints: {
          sm: 0,
          md: 768,
          tabletdown: 1023,
          lg: 1024
        }
      }
    ]
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.BASE_URL || 'https://fancysquares.blog/wp-json/',
    credentials: false,
    proxy: true,
  },
  // proxy
  proxy: {
    '/contact-us/': { target: 'https://portfolio.fancyapps.blog/gravityformsapi/forms/1/submissions?api_key=60592f6c43', pathRewrite: {'^/contact-us/': ''}, changeOrigin: true },
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
   vendor: [
    'jquery'
   ],
  plugins: [
     new webpack.ProvidePlugin({
       $: 'jquery',
       jQuery: 'jquery',
       'window.jQuery': 'jquery'
     })
   ],
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
