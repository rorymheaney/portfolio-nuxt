# rory-heaney

> Portfolio

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
$ cd directory
$ pm2 start npm --name "rory-heaney" -- start
$ or
$ pm2 start npm -i 2 --name "rory-heaney" -- start
$ cluster mode: pm2 start npm -i 2 --name "rory-heaney" -- start

# pm2 operations
$ cd directory
$ pm2 stop ID / all
$ pm2 delete ID / all

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
ln -sf /etc/nginx/sites-available/fancysquares.blog /etc/nginx/sites-enabled/fancysquares.blog

certbot --nginx -d fancysquares.blog -d www.fancysquares.blog